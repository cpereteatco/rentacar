﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using RentACar.API.App_Start;

namespace RentACar.API
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            IOC_Run.Run();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
       }
    }
}
