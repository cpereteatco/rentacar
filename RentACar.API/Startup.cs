﻿using System.Web.Http;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(RentACar.API.Startup))]
namespace RentACar.API
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=316888
            var config = new HttpConfiguration();
            WebApiConfig.Register(config);

            ConfigureAuthZero(app);
            app.UseCors(CorsOptions.AllowAll);
         
           //app.UseWebApi(config);
        }
    }
}
