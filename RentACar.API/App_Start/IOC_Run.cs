﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace RentACar.API.App_Start
{
    public static class IOC_Run
    {
        public static void Run()
        {
            SetAutofacWebAPIServices();
        }

        private static void SetAutofacWebAPIServices()
        {
            var configuration = GlobalConfiguration.Configuration;
            configuration.DependencyResolver = IOC.AutofacWebApiDependencyResolver();
          
        }
    }
}
