﻿using System.Data.Entity;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Http.Controllers;
using Autofac;
using Autofac.Integration.WebApi;
using RentACar.DataContext;
using RentACar.Repository;
using RentACar.RepositoryInterfaces;
using RentACar.Service;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.API.App_Start
{
    public static class IOC
    {
        public static AutofacWebApiDependencyResolver AutofacWebApiDependencyResolver()
        {
            var builder = new ContainerBuilder();

            // Register API controllers using assembly scanning.
            builder.RegisterApiControllers(Assembly.GetCallingAssembly());
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly())
                 .Where(t => typeof(IHttpController).IsAssignableFrom(t) && t.Name.EndsWith("Controller"));

            //DB Connection
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.Register<DbContext>(c => new RentACarDataContext());

            builder.Register(c => HttpContext.Current.User).As<IPrincipal>().InstancePerRequest();
           // builder.RegisterType<UserService>().As<IUserService>().InstancePerRequest();
            builder.RegisterType<PortfolioService>().As<IPortfolioService>().InstancePerRequest();
            builder.RegisterType<PricePeriodService>().As<IPricePeriodService>().InstancePerRequest();
            builder.RegisterType<CarService>().As<ICarService>().InstancePerRequest();
            builder.RegisterType<RentCarPeriodService>().As<IRentCarPeriodService>().InstancePerRequest();




            var container = builder.Build();

            // Set the dependency resolver implementation.
            var resolver = new AutofacWebApiDependencyResolver(container);
            return resolver;
        }
    }
}
