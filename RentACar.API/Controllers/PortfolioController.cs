﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.API.Controllers
{
    /// <summary>
    /// Portfolio Controller
    /// </summary>
   
    public class PortfolioController : ApiController
    {
        private readonly IPortfolioService _portfolioService;
        private readonly IPrincipal _principal;
       // string currentUserRole = ClaimsPrincipal.Current.FindFirst("groups").Value;
        /// <summary>
        /// Portfolio Constructor
        /// </summary>
        /// <param name="portfolioService"></param>
        public PortfolioController(IPortfolioService portfolioService, IPrincipal principal)
        {
            _portfolioService = portfolioService;
            _principal = principal;
        }

        /// <summary>
        /// Get All Portfolios 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var data = _portfolioService.GetPortfolios().ToList();
            return Request.CreateResponse(data.Any() ? HttpStatusCode.OK : HttpStatusCode.NoContent, data);
        }
        /// <summary>
        /// Get portfolio by id
        /// </summary>
        /// <param name="portfolioID"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        //   [Route("api/Portfolio/Get")]
        public HttpResponseMessage Get(int portfolioID)
        {

            var result = _portfolioService.GetPortfolio(portfolioID);        
            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
        }
        /// <summary>
        /// Get All portfolios by userId
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        //  [Route("api/Portfolio/Get/User")]
        public HttpResponseMessage GetByUserId(string userId)
        {
           // var userId = ClaimsPrincipal.Current.FindFirst("user_id").Value;
           
            var result = _portfolioService.GetPortfoliosByUserId(userId);
            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
        }

        /// <summary>
        /// Add Portfolio 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Post(PortfolioDTO request)
        {
            if (_principal.Identity.IsAuthenticated)
            {
                var userId = ClaimsPrincipal.Current.FindFirst("user_id").Value;
              

                    request.UserId = userId;
                    request.CreatedDate = DateTime.Now.ToUniversalTime();
                    request.ModifieDate = DateTime.Now.ToUniversalTime();
                    var response = _portfolioService.AddPortfolio(request);

                    return string.IsNullOrEmpty(response)
                        ? Request.CreateResponse(HttpStatusCode.OK, "Successfully saved")
                        : Request.CreateResponse(HttpStatusCode.BadRequest, response);
                }           
            return Request.CreateErrorResponse(HttpStatusCode.Unauthorized,"Unauthorized for this request");
        }

        /// <summary>
        /// Update Portfolio
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public HttpResponseMessage Put(PortfolioDTO request)
        {
            var userId = ClaimsPrincipal.Current.FindFirst("user_id").Value;
         
                request.UserId = userId;
              //  var currentPortfolio = _portfolioService.GetPortfolio(request.PortfolioId);
              //  currentPortfolio = request;
                var response = _portfolioService.UpdatePortfolio(request);
                return string.IsNullOrEmpty(response)
                    ? Request.CreateResponse(HttpStatusCode.OK, "Successfully updated")
                    : Request.CreateResponse(HttpStatusCode.BadRequest, response);

        }

        /// <summary>
        /// Delete Portfolio. Need to specify portfolio id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _portfolioService.DeletePortfolio(id);
                return Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, ex.Message);
                throw;
            }         
        }
    }
}
