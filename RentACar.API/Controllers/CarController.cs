﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;
using RentACar.Models.Models;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.API.Controllers
{
    /// <summary>
    /// Car Controller
    /// </summary>
    public class CarController : ApiController
    {
        private readonly ICarService _carService;
        private readonly IPrincipal _principal;


        public CarController(ICarService carService, IPrincipal principal)
        {
            _carService = carService;
            _principal = principal;
        }

        /// <summary>
        /// Get All Cars 
        /// </summary>
        /// <returns>List of all cars</returns>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var data = _carService.GetCars().ToList();
            return Request.CreateResponse(data.Any() ? HttpStatusCode.OK : HttpStatusCode.NoContent, data);
        }

        /// <summary>
        /// Get car by id
        /// </summary>
        /// <param name="carID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int carID)
        {
            var result = _carService.GetCar(carID);
            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
        }

        /// <summary>
        /// Get All cars by portfolioId
        /// </summary>
        /// <param name="portfolioID"></param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetByPortfolioId(int portfolioID)
        {
            var result = _carService.GetCarsByPortfolioId(portfolioID);
            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
        }

        public HttpResponseMessage GetByPortfolioName(string portfolioName)
        {
            var result = _carService.GetCarsByPortfolioName(portfolioName);
            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
        }

        /// <summary>
        /// Add Car 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Post(CarDTO request)
        {
            request.ModifiedDate = DateTime.Now.ToUniversalTime();
                var response = _carService.AddCar(request);
                return string.IsNullOrEmpty(response)
                    ? Request.CreateResponse(HttpStatusCode.OK, "Successfully saved")
                    : Request.CreateResponse(HttpStatusCode.BadRequest, response);
        }

        /// <summary>
        /// Update Car
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        [Authorize]
        public HttpResponseMessage Put(CarDTO request)
        {
           
                var currentCar = _carService.GetCar(request.CarId);
                currentCar = request;
                var response = _carService.UpdateCar(currentCar);
                return string.IsNullOrEmpty(response)
                    ? Request.CreateResponse(HttpStatusCode.OK, "Successfully updated")
                    : Request.CreateResponse(HttpStatusCode.BadRequest, response);
           
        }

        /// <summary>
        /// Delete Car. Need to specify portfolio id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Authorize]
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _carService.DeleteCar(id);
                return Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted");
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.MethodNotAllowed, ex.Message);
                throw;
            }
        }
    }
}
