﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.API.Controllers
{
    public class PricePeriodController : ApiController
    {
        private readonly IPricePeriodService _pricePeriodService;
        private readonly IPrincipal _principal;


        public PricePeriodController(IPricePeriodService pricePeriodService, IPrincipal principal)
        {
            _pricePeriodService = pricePeriodService;
            _principal = principal;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get()
        {
            var data = _pricePeriodService.GetPricePeriods().ToList();
            return Request.CreateResponse(data.Any() ? HttpStatusCode.OK : HttpStatusCode.NoContent, data);
        }

        [HttpGet]
        public HttpResponseMessage Get(int pricePeriodId)
        {
            var result = _pricePeriodService.GetPricePeriod(pricePeriodId);
            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
        }


        [HttpGet]
        public HttpResponseMessage GetByCarId(int carId)
        {
            var result = _pricePeriodService.GetPricePeriodsByCarId(carId);
            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
        }


        [HttpPost]
        [Authorize]
        public HttpResponseMessage Post(PricePeriodDTO request)
        {
            if (_principal.Identity.IsAuthenticated)
            {
                var userId = ClaimsPrincipal.Current.FindFirst("user_id").Value;


                request.UserID = userId;
                if (
                    _pricePeriodService.VerifyPeriodValidation(request.StartDatePeriod, request.EndDatePeriod,
                        request.CarID) == true)
                {
                    var response = _pricePeriodService.AddPricePeriod(request);
                    return string.IsNullOrEmpty(response)
                    ? Request.CreateResponse(HttpStatusCode.OK, "Successfully saved")
                    : Request.CreateResponse(HttpStatusCode.BadRequest, response);
                } 
            }
            return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized for this request");
        }       
    }
}
