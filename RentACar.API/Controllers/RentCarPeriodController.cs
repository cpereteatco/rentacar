﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Web.Http;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.API.Controllers
{
    public class RentCarPeriodController : ApiController
    {
        private readonly IRentCarPeriodService _rentCarService;
        private readonly IPricePeriodService _pricePeriodService;
        private readonly IPrincipal _principal;
    
        public RentCarPeriodController(IRentCarPeriodService rentCarService, IPrincipal principal, IPricePeriodService pricePeriodService)
        {
            _rentCarService =rentCarService;
            _pricePeriodService = pricePeriodService;
            _principal = principal;
        }
        [HttpPost]
        [Authorize]
        public HttpResponseMessage Post(RentCarPeriodDTO request)
        {
            if (_principal.Identity.IsAuthenticated)
            {
                var userId = ClaimsPrincipal.Current.FindFirst("user_id").Value;

                request.UserID = userId;
                request.NumberOfDays = 0;
                request.TotalPrice= _pricePeriodService.FindDateCadran(request.StartDate, request.CarID);
                request.CarAviability = false;
                var response = _rentCarService.AddRentPeriod(request);

                return string.IsNullOrEmpty(response)
                        ? Request.CreateResponse(HttpStatusCode.OK, "Successfully saved")
                        : Request.CreateResponse(HttpStatusCode.BadRequest, response);

            }
            return Request.CreateErrorResponse(HttpStatusCode.Unauthorized, "Unauthorized for this request");
        }


    }
        }
