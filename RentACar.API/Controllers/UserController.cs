﻿//using System;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using System.Web.Http.Description;
//using RentACar.ServiceInterface.DTO;
//using RentACar.ServiceInterface.ServiceInterface;

//namespace RentACar.API.Controllers
//{
//    /// <summary>
//    /// User Controller
//    /// </summary>
//    public class UserController : ApiController
//    {
//        private readonly IUserService _userService;
    
//        /// <summary>
//        /// User controller Constructor
//        /// </summary>
//        /// <param name="userService"></param>
//        public UserController(IUserService userService)
//        {
//            _userService = userService;
//        }

//        /// <summary>
//        /// Get All users
//        /// </summary>
//        /// <returns></returns>
//        [HttpGet]
//        public HttpResponseMessage Get()
//        {
           
//            var data = _userService.GetUsers().ToList();     
//            return Request.CreateResponse(data.Any() ? HttpStatusCode.OK : HttpStatusCode.NoContent, data);
//        }
//        /// <summary>
//        /// Get user by Id
//        /// </summary>
//        /// <param name="userID"></param>
//        /// <returns></returns>
//        [HttpGet]
//        [Route("api/User/Get")]
//        public HttpResponseMessage Get(int userID)
//        {
//            var result = _userService.GetUser(userID);
//            return Request.CreateResponse(result != null ? HttpStatusCode.OK : HttpStatusCode.NoContent, result);
//        }

//        /// <summary>
//        /// Add user 
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        [HttpPost]
//        public HttpResponseMessage Post(UserDTO request)
//        {            
//            if (ModelState.IsValid)
//            {
//                var response = _userService.AddUser(request);
//                return string.IsNullOrEmpty(response)
//                    ? Request.CreateResponse(HttpStatusCode.OK, "Successfully saved")
//                    : Request.CreateResponse(HttpStatusCode.BadRequest, response);
//            }
//            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
//        }

//        /// <summary>
//        /// Update user
//        /// </summary>
//        /// <param name="request"></param>
//        /// <returns></returns>
//        [HttpPut]
//        public HttpResponseMessage Put(UserDTO request)
//        {
//            if (ModelState.IsValid)
//            {
//                var currentUser = _userService.GetUser(request.UserId);
//                currentUser = request;
//                var response = _userService.UpdateUser(currentUser);
//                return string.IsNullOrEmpty(response)
//                    ? Request.CreateResponse(HttpStatusCode.OK, "Successfully updated")
//                    : Request.CreateResponse(HttpStatusCode.BadRequest, response);
//            }
//            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
//        }

//        /// <summary>
//        /// Delete User. Need to specify user id
//        /// </summary>
//        /// <param name="userId"></param>
//        /// <returns></returns>
//        [HttpDelete]
//        public HttpResponseMessage Delete(int userId)
//        {
//            var userToDelete =  _userService.GetUser(userId);
//            _userService.DeleteUser(userToDelete);
//            return Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted");
//        }

//    }
//}
