﻿using System;

namespace RentACar.ServiceInterface.DTO
{
    public class PricePeriodDTO
    {
        public int PricePeriodId { get; set; }

        public int PriceID { get; set; }
        public string PriceValue { get; set; }

        public int PeriodID { get; set; }
        public string StartDatePeriod { get; set; }
        public string EndDatePeriod { get; set; } 

        public int CarID { get; set; }
        public string UserID { get; set; }
    }
}
