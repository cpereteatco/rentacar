﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using RentACar.Models.Models;

namespace RentACar.ServiceInterface.DTO
{
    public class PortfolioDTO
    {
        public PortfolioDTO()
        {
            Cars = new List<Car>();
        }

        public int PortfolioId { get; set; }
        [Required]
        public DateTime CreatedDate { get; set; }
        public DateTime ModifieDate { get; set; }
        [Required]
        public string Name { get; set; }
        public string UserId { get; set; }

        public virtual ICollection<Car> Cars { get; set; }

    }
}