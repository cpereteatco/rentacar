﻿using System;
using System.Collections.Generic;
using RentACar.Models.Models;

namespace RentACar.ServiceInterface.DTO
{
    public class CarDTO
    {
        public CarDTO()
        {
            SubmittedDate = DateTime.Now.ToUniversalTime();
        }
     
        public int CarId { get; set; }
        public DateTime SubmittedDate { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsAvailable { get; set; }

        public virtual ICollection<CarPhoto> CarPhotos { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PricePeriod> PricePeriods { get; set; }
        public int PortfolioID { get; set; }

    }
}