﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentACar.ServiceInterface.DTO
{
    public class UserDTO
    {
        public UserDTO()
        {
            Portfolios = new List<PortfolioDTO>();
        }

        public int UserId { get; set; }

        [Required(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [EmailAddress]
        public string Email { get; set; }
        public string PasswordHash { get; set; }

        public virtual ICollection<PortfolioDTO> Portfolios { get; set; }
    }
}
