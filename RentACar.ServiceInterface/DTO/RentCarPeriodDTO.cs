﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RentACar.Models.Models;

namespace RentACar.ServiceInterface.DTO
{
    public class RentCarPeriodDTO
    {
        public int RentCarPeriodId { get; set; }
        public bool CarAviability { get; set; }
        public int NumberOfDays { get; set; }
        public double TotalPrice { get; set; }
        public int CarID { get; set; }
        public string UserID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

    }
}
