﻿using System;
using System.Collections.Generic;
using RentACar.ServiceInterface.DTO;

namespace RentACar.ServiceInterface.ServiceInterface
{
    public interface IPricePeriodService
    {
        IEnumerable<PricePeriodDTO> GetPricePeriods();
        PricePeriodDTO GetPricePeriod(int id);
        IEnumerable<PricePeriodDTO> GetPricePeriodsByCarId(int carId);
        string AddPricePeriod(PricePeriodDTO pricePeriod);
        void DeletePricePeriod(int id);
        double FindDateCadran(DateTime date,int carId);

        bool VerifyPeriodValidation(string startDate, string endDate, int carId);
    }
}
