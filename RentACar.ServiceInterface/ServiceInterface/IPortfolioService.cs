﻿using System.Collections.Generic;
using RentACar.ServiceInterface.DTO;

namespace RentACar.ServiceInterface.ServiceInterface
{
    public interface IPortfolioService
    {
        IEnumerable<PortfolioDTO> GetPortfolios();
        PortfolioDTO GetPortfolio(int id);
        IEnumerable<PortfolioDTO> GetPortfoliosByUserId(string userID);
        string AddPortfolio(PortfolioDTO portfolio);
        string UpdatePortfolio(PortfolioDTO portfolio);
        void DeletePortfolio(int id);
    }
}
