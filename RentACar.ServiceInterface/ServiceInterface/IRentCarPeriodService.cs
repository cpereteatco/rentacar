﻿using RentACar.ServiceInterface.DTO;

namespace RentACar.ServiceInterface.ServiceInterface
{
    public interface IRentCarPeriodService
    {
        string AddRentPeriod(RentCarPeriodDTO rent);
    }
}
