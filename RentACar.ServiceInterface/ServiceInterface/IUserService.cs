﻿using System.Collections.Generic;
using RentACar.ServiceInterface.DTO;

namespace RentACar.ServiceInterface.ServiceInterface
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetUsers();
        UserDTO GetUser(int id);
        string AddUser(UserDTO user);
        string UpdateUser(UserDTO user);
        void DeleteUser(UserDTO user);

    }
}
