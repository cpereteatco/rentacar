﻿using System.Collections.Generic;
using RentACar.ServiceInterface.DTO;

namespace RentACar.ServiceInterface.ServiceInterface
{
    public interface ICarService
    {
        IEnumerable<CarDTO> GetCars();
        CarDTO GetCar(int id);
        IEnumerable<CarDTO> GetCarsByPortfolioId(int portfolioId);
        string AddCar(CarDTO car);
        string UpdateCar(CarDTO car);
        void DeleteCar(int id);
        IEnumerable<CarDTO> GetCarsByPortfolioName(string portfolioName);
    }
}