﻿using System.Linq;

namespace RentACar.RepositoryInterfaces
{
    public interface IRepository<T> where T : class
    {
        void Insert(T entity);
        void Update(T entity);
        void Remove(T entity);
        IQueryable<T> GetAll();
    }
}
