﻿using System;

namespace RentACar.RepositoryInterfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<TEntity> Repository<TEntity>() where TEntity : class;

        void BeginTransaction();
        void CommitTransaction();
        void RollbackTransaction();

        int SaveChanges();
    }
}
