namespace RentACar.DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNameToCar : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Car", "Name", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Car", "Name");
        }
    }
}
