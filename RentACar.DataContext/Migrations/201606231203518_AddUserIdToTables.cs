namespace RentACar.DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserIdToTables : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PricePeriod", "UserID", c => c.String(nullable: false));
            AddColumn("dbo.RentCarPeriods", "UserId", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RentCarPeriods", "UserId");
            DropColumn("dbo.PricePeriod", "UserID");
        }
    }
}
