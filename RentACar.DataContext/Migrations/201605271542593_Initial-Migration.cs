namespace RentACar.DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarPhoto",
                c => new
                    {
                        PhotoId = c.Guid(nullable: false),
                        FileName = c.String(nullable: false, maxLength: 100),
                        FileType = c.String(nullable: false, maxLength: 50),
                        CarID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PhotoId)
                .ForeignKey("dbo.Car", t => t.CarID, cascadeDelete: true)
                .Index(t => t.CarID);
            
            CreateTable(
                "dbo.Car",
                c => new
                    {
                        CarId = c.Int(nullable: false, identity: true),
                        SubmittedDate = c.DateTime(nullable: false),
                        Year = c.Int(nullable: false),
                        Type = c.String(nullable: false, maxLength: 500, unicode: false),
                        Model = c.String(),
                        State = c.String(maxLength: 500, unicode: false),
                        ModifiedDate = c.DateTime(),
                        IsAvailable = c.Boolean(nullable: false),
                        PortfolioID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CarId)
                .ForeignKey("dbo.Portfolio", t => t.PortfolioID, cascadeDelete: true)
                .Index(t => t.PortfolioID);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Text = c.String(nullable: false, maxLength: 500),
                        CreatedOn = c.DateTime(nullable: false),
                        CarID = c.Int(),
                        AddedByUserID = c.String(),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Car", t => t.CarID)
                .Index(t => t.CarID);
            
            CreateTable(
                "dbo.Portfolio",
                c => new
                    {
                        PortfolioId = c.Int(nullable: false, identity: true),
                        CreatedDate = c.DateTime(nullable: false),
                        ModifiedDate = c.DateTime(),
                        Name = c.String(nullable: false, maxLength: 100),
                        UserID = c.String(nullable: false),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.PortfolioId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.PricePeriod",
                c => new
                    {
                        PricePeriodId = c.Int(nullable: false, identity: true),
                        PriceID = c.Int(nullable: false),
                        PeriodID = c.Int(nullable: false),
                        Car_CarId = c.Int(),
                    })
                .PrimaryKey(t => t.PricePeriodId)
                .ForeignKey("dbo.Period", t => t.PeriodID, cascadeDelete: true)
                .ForeignKey("dbo.Price", t => t.PriceID, cascadeDelete: true)
                .ForeignKey("dbo.Car", t => t.Car_CarId)
                .Index(t => t.PriceID)
                .Index(t => t.PeriodID)
                .Index(t => t.Car_CarId);
            
            CreateTable(
                "dbo.Period",
                c => new
                    {
                        PeriodId = c.Int(nullable: false, identity: true),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.PeriodId);
            
            CreateTable(
                "dbo.Price",
                c => new
                    {
                        PriceId = c.Int(nullable: false, identity: true),
                        PriceValue = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.PriceId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        RoleId = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false, maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserRoleId = c.Int(nullable: false, identity: true),
                        RoleID = c.Int(nullable: false),
                        UserID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserRoleId)
                .ForeignKey("dbo.Role", t => t.RoleID, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserID, cascadeDelete: true)
                .Index(t => t.RoleID)
                .Index(t => t.UserID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        Email = c.String(nullable: false, maxLength: 50, unicode: false),
                        PasswordHash = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "UserID", "dbo.Users");
            DropForeignKey("dbo.Portfolio", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RoleID", "dbo.Role");
            DropForeignKey("dbo.PricePeriod", "Car_CarId", "dbo.Car");
            DropForeignKey("dbo.PricePeriod", "PriceID", "dbo.Price");
            DropForeignKey("dbo.PricePeriod", "PeriodID", "dbo.Period");
            DropForeignKey("dbo.Car", "PortfolioID", "dbo.Portfolio");
            DropForeignKey("dbo.Comment", "CarID", "dbo.Car");
            DropForeignKey("dbo.CarPhoto", "CarID", "dbo.Car");
            DropIndex("dbo.UserRoles", new[] { "UserID" });
            DropIndex("dbo.UserRoles", new[] { "RoleID" });
            DropIndex("dbo.PricePeriod", new[] { "Car_CarId" });
            DropIndex("dbo.PricePeriod", new[] { "PeriodID" });
            DropIndex("dbo.PricePeriod", new[] { "PriceID" });
            DropIndex("dbo.Portfolio", new[] { "User_UserId" });
            DropIndex("dbo.Comment", new[] { "CarID" });
            DropIndex("dbo.Car", new[] { "PortfolioID" });
            DropIndex("dbo.CarPhoto", new[] { "CarID" });
            DropTable("dbo.Users");
            DropTable("dbo.UserRoles");
            DropTable("dbo.Role");
            DropTable("dbo.Price");
            DropTable("dbo.Period");
            DropTable("dbo.PricePeriod");
            DropTable("dbo.Portfolio");
            DropTable("dbo.Comment");
            DropTable("dbo.Car");
            DropTable("dbo.CarPhoto");
        }
    }
}
