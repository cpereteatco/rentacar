namespace RentACar.DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ADDCarIdToPricePeriod : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PricePeriod", "Car_CarId", "dbo.Car");
            DropIndex("dbo.PricePeriod", new[] { "Car_CarId" });
            RenameColumn(table: "dbo.PricePeriod", name: "Car_CarId", newName: "CarID");
            AlterColumn("dbo.PricePeriod", "CarID", c => c.Int(nullable: false));
            CreateIndex("dbo.PricePeriod", "CarID");
            AddForeignKey("dbo.PricePeriod", "CarID", "dbo.Car", "CarId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PricePeriod", "CarID", "dbo.Car");
            DropIndex("dbo.PricePeriod", new[] { "CarID" });
            AlterColumn("dbo.PricePeriod", "CarID", c => c.Int());
            RenameColumn(table: "dbo.PricePeriod", name: "CarID", newName: "Car_CarId");
            CreateIndex("dbo.PricePeriod", "Car_CarId");
            AddForeignKey("dbo.PricePeriod", "Car_CarId", "dbo.Car", "CarId");
        }
    }
}
