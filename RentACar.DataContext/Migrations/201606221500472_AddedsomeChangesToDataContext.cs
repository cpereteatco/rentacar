namespace RentACar.DataContext.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedsomeChangesToDataContext : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RentCarPeriods",
                c => new
                    {
                        RentCarPeriodId = c.Int(nullable: false, identity: true),
                        CarAviability = c.Boolean(nullable: false),
                        NumberOfDays = c.Int(nullable: false),
                        TotalPrice = c.Double(nullable: false),
                        CarID = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.RentCarPeriodId)
                .ForeignKey("dbo.Car", t => t.CarID, cascadeDelete: true)
                .Index(t => t.CarID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RentCarPeriods", "CarID", "dbo.Car");
            DropIndex("dbo.RentCarPeriods", new[] { "CarID" });
            DropTable("dbo.RentCarPeriods");
        }
    }
}
