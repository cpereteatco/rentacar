﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using RentACar.Models.Models;
using RentACar.Models.Models.Mappings;

namespace RentACar.DataContext
{
    public class RentACarDataContext : DbContext
    {
        public RentACarDataContext()
            : base("RentACarDatabase")
        {
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<CarPhoto> CarPhotos { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<Price> Prices { get; set; }
        public DbSet<PricePeriod> PricePeriods { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<UserRoles> UserRoles { get; set; }
        public DbSet<RentCarPeriods> RentCarPeriods { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new CarMap());
            modelBuilder.Configurations.Add(new CarPhotoMap());
            modelBuilder.Configurations.Add(new CommentMap());
            modelBuilder.Configurations.Add(new PeriodMap());
            modelBuilder.Configurations.Add(new PortfolioMap());
            modelBuilder.Configurations.Add(new PriceMap());
            modelBuilder.Configurations.Add(new PricePeriodMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserRolesMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new RentCarPeriodsMap());

        }
    }
}

