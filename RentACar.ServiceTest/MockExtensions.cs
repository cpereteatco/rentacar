﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace RentACar.ServiceTest
{
    public static class MockExtensions
    {
        public static void SetupIQueryable<T>(this Mock<DbSet<T>> mock, IQueryable<T> queryable)
        where T : class
        {
            mock.As<IQueryable<T>>().Setup(r => r.GetEnumerator()).Returns(queryable.GetEnumerator());
            mock.As<IQueryable<T>>().Setup(r => r.Provider).Returns(queryable.Provider);
            mock.As<IQueryable<T>>().Setup(r => r.ElementType).Returns(queryable.ElementType);
            mock.As<IQueryable<T>>().Setup(r => r.Expression).Returns(queryable.Expression);
        }
    }
}
