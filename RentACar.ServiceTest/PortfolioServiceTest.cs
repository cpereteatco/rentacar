﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Autofac;
using Moq;
using NUnit.Framework;
using RentACar.DataContext;
using RentACar.Models.Models;
using RentACar.Repository;
using RentACar.RepositoryInterfaces;
using RentACar.Service;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.ServiceTest
{
    [TestFixture]
    class PortfolioServiceTest
    {
        private IPortfolioService _portfolioService;
        private IContainer _diContainer;

        private void DependencySetup<T>(IEnumerable<T> list) where T : class
        {

            var model = list.AsQueryable();

            //mock dbset
            var modelMock = new Mock<DbSet<T>>();
            modelMock.SetupIQueryable(model);

            //mock database context
            var mockContext = new Mock<RentACarDataContext>();
            mockContext.Setup(c => c.Set<T>()).Returns(modelMock.Object);

            var builder = new ContainerBuilder();
            builder.RegisterInstance(mockContext.Object).As<DbContext>();

            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.Repository<T>()).Returns(new Repository<T>(mockContext.Object));

            builder.RegisterInstance(mockUnitOfWork.Object).As<IUnitOfWork>();

            _diContainer = builder.Build();
        }

        [Test]
        public void GetAllPortfolios_FullRepository_ReturnsSuccess()
        {
            //Arrange
            var portfolioList = new List<Portfolio>
            {
               new Portfolio {Name = "lala",PortfolioId = 1,CreatedDate = DateTime.Now,ModifieDate = DateTime.Now}
            };

            DependencySetup(portfolioList);

            _portfolioService = new PortfolioService(_diContainer.Resolve<IUnitOfWork>());

            //act
            var portfolios = _portfolioService.GetPortfolios().ToList();
            

            //assert
            Assert.IsNotNull(portfolios);
            Assert.AreEqual(portfolios.Count, 1);
        }

        [Test]
        public void GetPortfolioById_ReturnPortfolio()
        {
            // Arrange
            var portfolioList = new List<Portfolio>
            {
               new Portfolio {Name = "lala",PortfolioId = 1,CreatedDate = DateTime.Now,ModifieDate = DateTime.Now}
            };

            DependencySetup(portfolioList);

            _portfolioService = new PortfolioService(_diContainer.Resolve<IUnitOfWork>());

            //  act
            var portfolio = _portfolioService.GetPortfolio(1);

            // assert
            Assert.AreEqual(portfolio.Name.Equals("lala"), true);
        }

        [Test]
        public void InsertPortfolio_ReturnsSuccess()
        {
            //Arrange
            var portfolioList = new List<Portfolio>
            {
               new Portfolio {Name = "lala",PortfolioId = 1,CreatedDate = DateTime.Now,ModifieDate = DateTime.Now}
            };

            var portfolioToInsert = new PortfolioDTO
            {
                Name = "lala1",
                CreatedDate = DateTime.Now,
                ModifieDate = DateTime.Now
            };

            DependencySetup(portfolioList);

            _portfolioService = new PortfolioService(_diContainer.Resolve<IUnitOfWork>());

            //act
            _portfolioService.AddPortfolio(portfolioToInsert);
            

            //assert
             Assert.AreEqual(portfolioList.Count,2);
            }         
        }
    }
