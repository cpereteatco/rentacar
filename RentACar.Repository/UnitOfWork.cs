﻿using System;
using System.Data.Entity;
using RentACar.RepositoryInterfaces;

namespace RentACar.Repository
{
    public class UnitOfWork : IUnitOfWork
    {

        internal DbContext Context;
        private DbContextTransaction _transaction;

        public UnitOfWork(DbContext context)
        {
            Context = context;
        }

        public IRepository<TEntity> Repository<TEntity>() where TEntity : class
        {
            var repositoryType = typeof(Repository<>);
            return (IRepository<TEntity>)Activator.CreateInstance(repositoryType.MakeGenericType(typeof(TEntity)), Context);
        }

        public int SaveChanges()
        {
            return Context.SaveChanges();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;
            _transaction?.Dispose();

            Context.Dispose();
        }

        public void BeginTransaction()
        {
            _transaction = Context.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            _transaction.Commit();
        }

        public void RollbackTransaction()
        {
            _transaction.Rollback();
        }
    }
}
