﻿using System.Data.Entity;
using System.Linq;
using RentACar.RepositoryInterfaces;

namespace RentACar.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbContext _db;
        protected readonly DbSet<T> ObjectSet;

        public Repository(DbContext db)
        {
            _db = db;
            this.ObjectSet = db.Set<T>();
        }
        public void Insert(T entity)
        {
            _db.Set<T>().Add(entity);
        }
        public void Update(T entity)
        {
            _db.Set<T>().Attach(entity);     
            _db.Entry(entity).State = EntityState.Modified;
        }
        public IQueryable<T> GetAll()
        {
            return _db.Set<T>();
        }
        public void Remove(T entity)
        {
            _db.Entry(entity).State = EntityState.Deleted;

        }
    }
}
