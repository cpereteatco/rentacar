﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using AutoMapper;
//using RentACar.Models.Models;
//using RentACar.RepositoryInterfaces;
//using RentACar.ServiceInterface.DTO;
//using RentACar.ServiceInterface.ServiceInterface;

//namespace RentACar.Service
//{
//    public class UserService: IUserService
//    {
//        private readonly IUnitOfWork _unitOfWork;
//        private readonly IRepository<User> _userRepository;

//        public UserService(IUnitOfWork unitOfWork)
//        {
//            _unitOfWork = unitOfWork;
//            _userRepository = _unitOfWork.Repository<User>();
//        }

//        public IEnumerable<UserDTO> GetUsers()
//        {
//            var users = _userRepository.GetAll().ToList();
//            var items = Mapper.DynamicMap<List<User>, List<UserDTO>>(users);
//            return items;          
//        }

//        public UserDTO GetUser(int id)
//        {
//            var user = _userRepository.GetAll().FirstOrDefault(x => x.UserId == id);
//            var mappedUser = Mapper.DynamicMap<User, UserDTO>(user);
//            return mappedUser;            
//        }
//        public bool VerifyUser(UserDTO user)
//        {
//            var foundUser = _userRepository.GetAll().FirstOrDefault(t => t.UserId == user.UserId);
//            if (foundUser == null) throw new ArgumentNullException(nameof(foundUser));
//            //Verify user password
//            bool passwordIsCorrect = BCrypt.Net.BCrypt.Verify(user.PasswordHash,foundUser.PasswordHash);
//            return passwordIsCorrect;
//        }
//        public string AddUser(UserDTO user)
//        {
//            user.PasswordHash = BCrypt.Net.BCrypt.HashPassword(user.PasswordHash);
//            var mappedUser = Mapper.DynamicMap<UserDTO, User>(user);
//            _userRepository.Insert(mappedUser);
//            _unitOfWork.SaveChanges();
//            return string.Empty;
//        }

//        public string UpdateUser(UserDTO user)
//        {
//            user.PasswordHash = BCrypt.Net.BCrypt.HashPassword(user.PasswordHash);
//            var mappedUser = Mapper.DynamicMap<UserDTO, User>(user);
//            _userRepository.Update(mappedUser);
//            _unitOfWork.SaveChanges();
//            return string.Empty;
//        }
//        public void DeleteUser(UserDTO user)
//        {
//            var mappedUser = Mapper.DynamicMap<UserDTO, User>(user);
//            _userRepository.Remove(mappedUser);
//            _unitOfWork.SaveChanges();

//        }
//    }
//}
