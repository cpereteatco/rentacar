﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using RentACar.Models.Models;
using RentACar.RepositoryInterfaces;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.Service
{
    public class PortfolioService:IPortfolioService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Portfolio> _portfolioRepository;
       
        public PortfolioService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _portfolioRepository = _unitOfWork.Repository<Portfolio>();           
        }
        public IEnumerable<PortfolioDTO> GetPortfolios()
        {
            var portfolios = _portfolioRepository.GetAll().ToList();
            var items = Mapper.DynamicMap<List<Portfolio>, List<PortfolioDTO>>(portfolios);
            return items;
        }

        public PortfolioDTO GetPortfolio(int id)
        {
            var portfolio = _portfolioRepository.GetAll().FirstOrDefault(p => p.PortfolioId == id);
            var mappedPortfolio= Mapper.DynamicMap<Portfolio, PortfolioDTO>(portfolio);
            return mappedPortfolio;
        }

        public IEnumerable<PortfolioDTO> GetPortfoliosByUserId(string userID)
        {
            var portfolios = _portfolioRepository.GetAll().Where(p => p.UserID == userID).ToList();
            var items = Mapper.DynamicMap<List<Portfolio>, List<PortfolioDTO>>(portfolios);
            return items;         
        }

        public string AddPortfolio(PortfolioDTO portfolio)
        {
            var mappedPortfolio = Mapper.DynamicMap<PortfolioDTO, Portfolio>(portfolio);
            _portfolioRepository.Insert(mappedPortfolio);
            _unitOfWork.SaveChanges();
            return string.Empty;
        }

        public string UpdatePortfolio(PortfolioDTO portfolio)
        {
          
            var mappedPortfolio = Mapper.DynamicMap<PortfolioDTO, Portfolio>(portfolio);
            _portfolioRepository.Update(mappedPortfolio);
            _unitOfWork.SaveChanges();
            return string.Empty;
        }

        public void DeletePortfolio(int id)
        {          
            var portfolioToDelete = _portfolioRepository.GetAll().FirstOrDefault(x => x.PortfolioId == id);
            _portfolioRepository.Remove(portfolioToDelete);
            _unitOfWork.SaveChanges();

        }
    }
}
