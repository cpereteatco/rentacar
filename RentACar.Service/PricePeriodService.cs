﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mail;
using AutoMapper;
using RentACar.Models.Models;
using RentACar.RepositoryInterfaces;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.Service
{
    public class PricePeriodService :IPricePeriodService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Price> _priceRepository;
        private readonly IRepository<Period> _periodRepository;
        private readonly IRepository<PricePeriod> _pricePeriodRepository;
        public PricePeriodService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _pricePeriodRepository = _unitOfWork.Repository<PricePeriod>();
            _periodRepository = _unitOfWork.Repository<Period>();
            _priceRepository = _unitOfWork.Repository<Price>();
        }

        public IEnumerable<PricePeriodDTO> GetPricePeriods()
        {
            var pricePeriods = _pricePeriodRepository.GetAll().ToList();
            var items = Mapper.DynamicMap<List<PricePeriod>, List<PricePeriodDTO>>(pricePeriods);
            return items;
        }

        public PricePeriodDTO GetPricePeriod(int id)
        {
            var pricePeriod = _pricePeriodRepository.GetAll().FirstOrDefault(p => p.PricePeriodId == id);
            var mappedPortfolio = Mapper.DynamicMap<PricePeriod, PricePeriodDTO>(pricePeriod);
            return mappedPortfolio;
        }

        public IEnumerable<PricePeriodDTO> GetPricePeriodsByCarId(int carId)
        {
            List<PricePeriodDTO> pricePeriodDtos = new List<PricePeriodDTO>();
            var pricePeriodByCarId = _pricePeriodRepository.GetAll().Where(p => p.CarID == carId).ToList();
            foreach (var item in pricePeriodByCarId)
            {
                var price = item.Price;
                var period = item.Period;

                var pricePeriodDTO = new PricePeriodDTO
                {
                    CarID = carId,
                    EndDatePeriod = period.EndDate.ToShortDateString(),
                    StartDatePeriod = period.StartDate.ToShortDateString(),
                    PricePeriodId = item.PricePeriodId,
                    PriceValue = price.PriceValue.ToString()
                };
                pricePeriodDtos.Add(pricePeriodDTO);
            }

            return pricePeriodDtos;
        }

        public string AddPricePeriod(PricePeriodDTO pricePeriod)
        {
            Price price = new Price {PriceValue = Convert.ToDouble(pricePeriod.PriceValue)};
            Period period = new Period {EndDate = Convert.ToDateTime(pricePeriod.EndDatePeriod), StartDate = Convert.ToDateTime(pricePeriod.StartDatePeriod)};
            _periodRepository.Insert(period);
            _priceRepository.Insert(price);

            var mappedPricePeriod = new PricePeriod {CarID = pricePeriod.CarID,PeriodID =period.PeriodId , PriceID = price.PriceId ,UserId = pricePeriod.UserID};
            _pricePeriodRepository.Insert(mappedPricePeriod);
            _unitOfWork.SaveChanges();
            return string.Empty;            
        }

        public void DeletePricePeriod(int id)
        {
            throw new System.NotImplementedException();
        }
        public double FindDateCadran(DateTime date,int carId)
        {
            var pricePeriods = GetPricePeriodsByCarId(carId);
            return (from pp in pricePeriods let isBetween = Convert.ToDateTime(date) >=
                    Convert.ToDateTime(pp.StartDatePeriod) 
                    && Convert.ToDateTime(date) < 
                    Convert.ToDateTime(pp.EndDatePeriod)
                    where isBetween == true
                    let difference = Convert.ToDateTime(pp.EndDatePeriod) - 
                    Convert.ToDateTime(date)
                    let days = difference.TotalDays
                    select days*Convert.ToDouble(pp.PriceValue)).Sum();
        }

        public bool VerifyPeriodValidation(string startDate, string endDate, int carId)
        {
            bool isValidDate = true;
            var pricePeriods = GetPricePeriodsByCarId(carId);
            foreach (var pp in pricePeriods)
            {
                var startDateIsBetween = DateIsBetween(Convert.ToDateTime(startDate),
                    Convert.ToDateTime(pp.StartDatePeriod), Convert.ToDateTime(pp.EndDatePeriod));
                var endDateIdBetween = DateIsBetween(Convert.ToDateTime(endDate),
                    Convert.ToDateTime(pp.StartDatePeriod), Convert.ToDateTime(pp.EndDatePeriod));
                if (startDateIsBetween) 
                {
                    isValidDate = false;
                }
                if (endDateIdBetween)
                {
                    isValidDate = false;
                }
            }
            return isValidDate;
        }

        private bool DateIsBetween(DateTime date,DateTime comparedDate1, DateTime comparedDate2)
        {
            if (date >= comparedDate1 && date <= comparedDate2)
            {
                return true;
            }
            return false;
        }
    }
}
