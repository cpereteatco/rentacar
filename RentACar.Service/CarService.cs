﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using RentACar.Models.Models;
using RentACar.RepositoryInterfaces;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.Service
{
    public class CarService:ICarService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Car> _carRepository;

        public CarService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _carRepository = _unitOfWork.Repository<Car>();
        }
        public IEnumerable<CarDTO> GetCars()
        {
            var cars = _carRepository.GetAll().ToList();
            var items = Mapper.DynamicMap<List<Car>, List<CarDTO>>(cars);
            return items;
        }

        public CarDTO GetCar(int id)
        {
            var car = _carRepository.GetAll().FirstOrDefault(p => p.CarId == id);
            var mappedCar = Mapper.DynamicMap<Car, CarDTO>(car);
            return mappedCar;
        }

        public IEnumerable<CarDTO> GetCarsByPortfolioId(int portfolioId)
        {
            var cars = _carRepository.GetAll().Where(p => p.PortfolioID == portfolioId).ToList();
            var items = Mapper.DynamicMap<List<Car>, List<CarDTO>>(cars);
            return items;
        }

        public string AddCar(CarDTO car)
        {
            var mappedCar= Mapper.DynamicMap<CarDTO, Car>(car);
            _carRepository.Insert(mappedCar);
            _unitOfWork.SaveChanges();
            return string.Empty;
        }

        public string UpdateCar(CarDTO car)
        {
            var mappedCar = Mapper.DynamicMap<CarDTO, Car>(car);
            _carRepository.Update(mappedCar);
            _unitOfWork.SaveChanges();
            return string.Empty;
        }

        public void DeleteCar(int id)
        {
            var carToDelete = _carRepository.GetAll().FirstOrDefault(x => x.CarId == id);
            _carRepository.Remove(carToDelete);
            _unitOfWork.SaveChanges();
        }

        public IEnumerable<CarDTO> GetCarsByPortfolioName(string portfolioName)
        {
            var cars = _carRepository.GetAll().Where(p => p.Portfolio.Name == portfolioName).ToList();
            var items = Mapper.DynamicMap<List<Car>, List<CarDTO>>(cars);
            return items;
        }
    }
}
