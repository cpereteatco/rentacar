﻿using System;
using AutoMapper;
using RentACar.Models.Models;
using RentACar.RepositoryInterfaces;
using RentACar.ServiceInterface.DTO;
using RentACar.ServiceInterface.ServiceInterface;

namespace RentACar.Service
{
    public class RentCarPeriodService : IRentCarPeriodService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<RentCarPeriods> _rentCarRepository;

        public RentCarPeriodService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _rentCarRepository = _unitOfWork.Repository<RentCarPeriods>();
        }

        public string AddRentPeriod(RentCarPeriodDTO rent)
        {
            var mappedRent = Mapper.DynamicMap<RentCarPeriodDTO, RentCarPeriods>(rent);
            _rentCarRepository.Insert(mappedRent);
            _unitOfWork.SaveChanges();
            return string.Empty;

        }
    }
}
