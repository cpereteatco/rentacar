(function () {
    'use strict';

    angular.module('app', [
        'auth0',
        'angular-storage',
        'angular-jwt',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'cgBusy',
        'ngSanitize'      
    ]);

})();
(function () {
    'use strict';

    angular.module('app')
        .constant('appSettings', {
            apiPath: 'http://localhost:31159/'
        })
        .config([
            '$urlRouterProvider', '$stateProvider', 'authProvider', '$httpProvider', '$locationProvider', 'jwtInterceptorProvider',
            function($urlRouterProvider, $stateProvider, authProvider, $httpProvider, $locationProvider,
                jwtInterceptorProvider) {
                $urlRouterProvider.otherwise('/');

                $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: 'app/components/home/home.html',
                        controller: 'HomeController',
                        requiresLogin: true
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'app/components/Login/login.html',
                        controller: 'LoginController'
                    })
                    .state('management', {
                        url: '/management',
                        templateUrl: 'app/components/Management/management.html',
                        controller: 'ManagementController'
                    })
                    .state('management.portfolio', {
                        url: '/portfolio',
                        templateUrl: 'app/components/Portfolio/portfolio-name_list.html',
                        controller: 'PortfolioController'
                    })
                    .state('management.cars', {
                        url: '/cars/:portfolioId',
                        templateUrl: 'app/components/Car/portfolio-cars-list.html',
                        controller: 'CarController'
                    })
                    .state('cars', {
                        url: '/viewCars',
                        templateUrl: 'app/components/Car/cars-list.html',
                        controller: 'CarController'
                    });

                authProvider.init({
                    domain: 'cpereteatco.eu.auth0.com',
                    clientID: 'qrXNCWbwZkfG2VFqDwlWxiTttv6MIhwv',
                    loginUrl: '/login',
                    loginState: 'login'
                });
                var refreshingToken = null;
                jwtInterceptorProvider.tokenGetter = function(store, jwtHelper) {
                    var token = store.get('token');
                    var refreshToken = store.get('refreshToken');
                    if (token) {
                        if (!jwtHelper.isTokenExpired(token)) {
                            return store.get('token');
                        } else {
                            if (refreshingToken === null) {
                                refreshingToken = auth.refreshIdToken(refreshToken).then(function(idToken) {
                                    store.set('token', idToken);
                                    return idToken;
                                }).finally(function() {
                                    refreshingToken = null;
                                });
                            }
                            return refreshingToken;
                        }
                    }
                };
                
                $httpProvider.interceptors.push('jwtInterceptor');
            }
        ])
.run(function ($rootScope, auth, store, jwtHelper, $state) {
    var refreshingToken = null;
            $rootScope.$on('$locationChangeStart', function(event,toState) {
                var token = store.get('token');
                var refreshToken = store.get('refreshToken');

                if (token) {
                    if (!jwtHelper.isTokenExpired(token)) {
                        if (!auth.isAuthenticated) {
                            $rootScope.isAuthenticated = true;
                            
                            auth.authenticate(store.get('profile'), token);
                            if ($rootScope.isAuthenticated) {
                                var profileIn = store.get('profile');
                                var profileRole = profileIn.authorization.groups[0];
                                if (profileRole === 'User') {
                                    $rootScope.isAdmin = false;

                                } else {
                                    $rootScope.isAdmin = true;
                                }                               
                            }
                        }
                    } else {
                        if (refreshToken) {
                            if (refreshingToken === null) {
                                refreshingToken = auth.refreshIdToken(refreshToken).then(function(idToken) {
                                    store.set('token', idToken);
                                    auth.authenticate(store.get('profile'), idToken);
                                }).finally(function() {
                                    refreshingToken = null;
                                });
                            }
                           
                            return refreshingToken;
                        } else {
                            $state.go('login');
                        }
                    }
                }
            });
    auth.hookEvents();
});
})();

(function () {
    'use strict';

    angular.module('app').factory('pricePeriodService', function (PricePeriod) {
        var pricePeriodService = {};
        pricePeriodService.pp = function () {
            return PricePeriod.query();
        };
        pricePeriodService.pp = function (id) {
            return PricePeriod.get({ id: id });
        };

        pricePeriodService.ppByCarId = function (carId) {
            return PricePeriod.query({ carId: carId });
        };

        return pricePeriodService;
    });

})();

(function () {
    'use strict';

    angular.module('app').factory('PricePeriod', function ($resource, authService, appSettings) {
        return $resource(appSettings.apiPath + 'api/priceperiod/:id', { id: '@id' },
            {
                'get': { method: 'GET' },
                'query': {
                    method: 'GET',
                    isArray: true
                },
                'save': { method: 'POST' },
                'remove': { method: 'DELETE' },
                'update': { method: 'PUT' }
            });
    });
})();

(function () {
    'use strict';

    angular.module('app').controller('RentCarController', function ($scope, car, pricePeriodService, $modalInstance,RentCar) {
        $scope.car = car;
           $scope.pricePeriods = function () {
                $scope.promise = pricePeriodService.ppByCarId($scope.car.CarId).$promise.then(function (prt) {
                    $scope.pp = prt;
                });
           };
           $scope.calendar = {
               opened: {},
               dateFormat: 'MM/dd/yyyy',
               dateOptions: {},
               open: function ($event, which) {
                   $event.preventDefault();
                   $event.stopPropagation();
                   $scope.calendar.opened[which] = true;
               }
           };
        $scope.claculateTotalPrice = function(startDate, endDate) {

        };

        $scope.submitForm = function () {
            $scope.dataToSend = { CarID: $scope.car.CarId, StartDate: $scope.periodstartDate, EndDate: $scope.periodendDate };
            RentCar.save($scope.dataToSend).$promise.then(function () {

                $modalInstance.close();
            });
        };

        $scope.cancelForm = function() {
            $modalInstance.dismiss();
        };
    });
})();
(function () {
    'use strict';

    angular.module('app').controller('CarModalController', function ($scope, $modal, Cars, carService, portfolioId,$modalInstance) {

        $scope.car = {
            PortfolioId: portfolioId,
            State: 'selected',
            Year:'selected'

        };

        $scope.carStatesList = [
        { name: 'New' },
        { name: 'Old' }
        ];

        $scope.years = [];
        for (var currentYear = new Date().getFullYear(), i = currentYear; currentYear - 40 < i; $scope.years.push(i--));

        

        $scope.submitForm = function () {
            Cars.save($scope.car).$promise.then(function () {

                $modalInstance.close();
            });
           
        };
        $scope.cancelForm = function() {
            $modalInstance.dismiss();
        };
    });
})();
(function () {
    'use strict';

    angular.module('app').factory('carService', function (Cars) {
        var carService = {};
        carService.cars = function () {
            return Cars.query();
        };
        carService.car = function (id) {
            return Cars.get({ id: id });
        };

        carService.carsByPortfolioName = function (portfolioName) {
            return Cars.query({ portfolioName: portfolioName });
        };
        carService.carsByPortfolioId = function (portfolioID) {
            return Cars.query({ portfolioId: portfolioID });
        };
       
        return carService;
    });

})();
(function () {
    'use strict';

    angular.module('app').controller('CarController', function ($scope, $state, store, $modal,
        carService, portfolioService, $stateParams) {

        $scope.initCarsController = function () {
         
            $scope.changePortfolio = function (selectedPortfolio) {
                $scope.portfolioId = selectedPortfolio.PortfolioId;
             //   $state.go('management.cars', { portfolioId: $scope.portfolioId });
                $scope.promise = carService.carsByPortfolioId($scope.portfolioId)
                    .$promise.then(function (cars) {
                   $scope.carsByPortfolio = cars;
                });
                
            };

            var profile = store.get('profile');
            var userId = profile.user_id;
            $scope.promise = portfolioService.portfoliosByUserId(userId).$promise.then(function (prt) {
                $scope.portfolios = prt;
            });

           $scope.addCar = function() {
               $modal.open({
                   size: 'lg',
                   templateUrl: 'app/components/Car/add-car.modal.html',
                   controller: 'CarModalController',
                   resolve: {
                       portfolioId: function() {
                           return $scope.portfolioId;
                       }
                   }
               });
           };

            $scope.deleteCar = function(car) {
                $modal.open({
                    templateUrl: 'app/components/Car/remove-car.modal.html',
                    resolve: {
                        car: function() {
                            return car;
                        },
                        allCars: function() {
                            return $scope.carsByPortfolio;
                        }
                    },
                    controller: function($scope, $modalInstance, car, allCars, Cars) {
                        $scope.cancelForm = function() {
                            $modalInstance.dismiss();
                        };
                        $scope.submitForm = function() {
                            Cars.remove({ id: car.CarId }).$promise.then(function() {
                                var index = allCars.indexOf(car);

                                allCars.splice(index, 1);
                            }, function(response) {

                                var status = response.status;
                                if (status === 400) {
                                    $modal.open({
                                        templateUrl: 'app/components/Core/error.cars.modal.html ',
                                        controller: function($scope, $modalInstance) {
                                            $scope.closeForm = function() {
                                                $modalInstance.dismiss();
                                            };

                                        }
                                    });
                                }
                            });
                            $modalInstance.close();
                        };
                    }
                });
            };

            $scope.editCar = function (car) {               
                $modal.open({
                    size: 'lg',
                    templateUrl: 'app/components/Car/edit-car.modal.html',
                    controller: 'EditCarController',
                    resolve: {
                        car: function() {
                            return car;
                        }
                    }
                });
            };

            //$scope.saveOrCancelCar = function (e, car) {
            //    if (e.keyCode === 13) {
            //        Portfolios.update({ id: portfolio.PortfolioId }, portfolio).$promise.then(function () {
            //            $scope.initPortfolioTable();
            //        });
            //        portfolio.isInEdit = false;
            //    }
            //    if (e.keyCode === 27) {
            //        this.portfolio = angular.copy($scope.originalPortfolio);
            //    }
            //};
        };
        $scope.rentCar = function (car) {           
            $modal.open({
                size: 'lg',
                templateUrl: 'app/components/Car/rent-car.modal.html',
                controller: 'RentCarController',
                resolve: {
                    car: function () {
                        return car;
                    }
                }
            });
        };
        $scope.initCars = function () {        
            $scope.promise = carService.cars().$promise.then(function (prt) {
                    $scope.cars = prt;
                });
        };
       
    });
})();
(function () {
    'use strict';

    angular.module('app').factory('Cars', function ($resource, authService, appSettings) {
        return $resource(appSettings.apiPath + 'api/car/:id', { id: '@id' },
            {
                'get': { method: 'GET' },
                'query': {
                    method: 'GET',
                    isArray: true
                },
                'save': { method: 'POST' },
                'remove': { method: 'DELETE' },
                'update': { method: 'PUT' }
            });
    });
})();

(function () {
    'use strict';

    angular.module('app').controller('EditCarController', function ($scope, $modalInstance, $modal, car, PricePeriod, pricePeriodService) {
        $scope.car = car;
        $scope.isDisabled = true;
        
        $scope.promise = pricePeriodService.ppByCarId(car.CarId)
            .$promise.then(function(pp) {
                $scope.pricePeriods = pp;
            });
        $scope.years = [];
        for (var currentYear = new Date().getFullYear(), i = currentYear; currentYear - 40 < i; $scope.years.push(i--));
        $scope.carStatesList = [
            { name: 'New' },
            { name: 'Old' }
        ];

        $scope.enableSubmit = function() {
            $scope.isDisabled = false;
        };
       
        $scope.calendar = {
            opened: {},
            dateFormat: 'MM/dd/yyyy',
            dateOptions: {},
            open: function($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };
        $scope.addpriceperiod = function (car) {
            var carId = car.CarId;
            console.log(carId);
            var pricePeriod = {
                PriceValue: $scope.priceValue, StartDatePeriod: $scope.periodstartDate, EndDatePeriod: $scope.periodendDate, CarID: carId,
                PeriodID:0,PriceID:0,PricePeriodId:0
            };
            PricePeriod.save(pricePeriod).$promise.then(function () {
                $scope.periodstartDate = '';
                $scope.periodendDate = '';
                    $scope.priceValue = '';
                })
            .catch(function (response) {
                console.error('Not Valid data', response.status, response.data);
                $scope.periodstartDate = '';
                $scope.periodendDate = '';
                $scope.priceValue = '';
            });

        };


        $scope.submitForm=function() {
            
        };
        $scope.cancelForm = function() {
            $modalInstance.dismiss();
        };
    });
})();

(function () {
    'use strict';

    angular.module('app').factory('RentCar', function ($resource, authService, appSettings) {
        return $resource(appSettings.apiPath + 'api/rentcarperiod/:id', { id: '@id' },
            {
                'get': { method: 'GET' },
                'query': {
                    method: 'GET',
                    isArray: true
                },
                'save': { method: 'POST' },
                'remove': { method: 'DELETE' },
                'update': { method: 'PUT' }
            });
    });
})();
(function() {
    'use strict';

    angular.module('app').factory('authService', function(authSession, auth,store) {
        var authService = {};
        authService.isAuthenticated = function() {
            if (!auth.isAuthenticated) {
                return false;
            } else {
                return true;
            }
        };

        authService.getRole = function() {
            var profile = store.get('profile');
            var role = profile.authorization.groups[0];
            return role;
        };
        return authService;
    });
})();

(function () {
    'use strict';

    angular.module('app').service('authSession', function () {
        this.create = function (sessionId) {
            this.id = sessionId;
        };
        this.destroy = function () {
            this.id = null;
        };
        return this;
    });
})();

(function() {
    'use strict';

    angular.module('app').controller('LoginController', function($scope,$window,$rootScope, auth, $state, store) {

        $scope.login = function() {
            auth.signin({
                authParams: {
                    scope: 'openid profile'
                }
            }, function(profile, token, access_token, state, refresh_token) {
                store.set('profile', profile);
                store.set('token', token);
                store.set('refreshToken', refresh_token);
                $window.location.reload();
                $state.go('home');
            }, function(error) {
                console.log("There was an error", error);
            });
        };

    });
    
})();

(function () {
    'use strict';

    angular.module('app').controller('ManagementController', function($scope, $state, auth, $rootScope) {
        $scope.active = function(route) {
            return $state.is(route);
        };     
        $scope.tabs = [
            { heading: 'Portfolios', route: 'management.portfolio', active: true },
            { heading: 'Cars', route: 'management.cars', active: false }
        ];
           
        $scope.$on('$stateChangeSuccess', function () { // Keep the right tab highlighted if the URL changes.
           
            $scope.tabs.forEach(function (tab) {
                tab.active = $scope.active(tab.route);
            });
        });
    });
})();

(function () {
    'use strict';

    angular.module('app').controller('HomeController', function ($scope, $window, authService,
        store, auth, $state, portfolioService) {
     
        $scope.initHomeCtrl = function () {
          
            $scope.interval = portfolioService.portfolios().$promise.then(function (prt) {
                $scope.portfolios = prt;
            });
           
        };

        $scope.selectPortfolio = function (portfolio) {
            $state.go('cars');
            store.set('portfolioID', portfolio.PortfolioId);
        };

        $scope.logout = function() {
            auth.signout();
            store.remove('profile');
            store.remove('token');
            store.remove('refreshToken');
            $window.location.reload();
        };
    });
   

})();
(function () {
    'use strict';

    angular.module('app').factory('portfolioService', function (Portfolios) {
        var portfolioService = {};
        portfolioService.portfolios = function () {
            return Portfolios.query();
        };
        portfolioService.portfolio = function (id) {
            return Portfolios.get({ id: id });
        };

        portfolioService.portfoliosByUserId = function (userId) {
            return Portfolios.query({ userId:userId });
        };
        //portfolioService.portfolio = function (portfolio) {
        //    return Portfolios.save(portfolio);
        //};

        return portfolioService;
    });

})();
(function () {
    'use strict';
    angular.module('app').controller('PortfolioController', function ($scope,$modal,store, Portfolios, portfolioService) {
        $scope.initPortfolioTable = function () {
            $scope.initPortfoliosForSpecificUser();
            $scope.newPortfolio = { Name: '' };
        };
     
        $scope.initPortfolios = function () {
            $scope.promise = portfolioService.portfolios().$promise.then(function (prt) {
                $scope.portfolios = prt;
            });
        };

        $scope.initPortfoliosForSpecificUser = function () {
            var profile = store.get('profile');
            var userId = profile.user_id;
            $scope.promise = portfolioService.portfoliosByUserId(userId).$promise.then(function (prt) {
                $scope.portfoliosByUserId = prt;
            });
        };
    
  
        $scope.deletePortfolio = function(portfolio) {
            $modal.open({
                templateUrl: 'app/components/portfolio/remove-portfolio.modal.html',
                resolve: {
                    portfolio: function () {
                        return portfolio;
                    },
                    allPortfolios: function() {
                        return $scope.portfoliosByUserId;
                    }
                },
                controller: function ($scope, $modalInstance, portfolio, allPortfolios, Portfolios) {
                    $scope.cancelForm = function() {
                        $modalInstance.dismiss();
                    };
                    $scope.submitForm = function() {
                        Portfolios.remove({ id: portfolio.PortfolioId }).$promise.then(function () {
                            var index =allPortfolios.indexOf(portfolio);

                            allPortfolios.splice(index,1);
                        }, function (response) {

                            var status = response.status;
                            if (status === 400) {
                               $modal.open({
                                    templateUrl: 'app/components/Core/error.portfolios.modal.html ',
                                    controller: function ($scope, $modalInstance) {
                                        $scope.closeForm = function() {
                                            $modalInstance.dismiss();
                                        };

                                    }
                                });
                            }
                        });
                        $modalInstance.close();
                    };
                }
            });
        };
        $scope.editPortfolio = function(portfolio) {
            var nothingInEdit = $scope.editedPortfolio !== undefined;
            if (nothingInEdit) {
                $scope.editedPortfolio.isInEdit = false;
            }           
            $scope.editedPortfolio = portfolio;
            $scope.originalPortfolio = angular.copy(portfolio);
            portfolio.isInEdit = true;
        };
        $scope.saveOrCancelPortfolio = function(e, portfolio) {
            if (e.keyCode === 13) {
                Portfolios.update({id: portfolio.PortfolioId}, portfolio).$promise.then(function() {
                    $scope.initPortfolioTable();
                });
                portfolio.isInEdit = false;
            }
            if (e.keyCode === 27) {
              this.portfolio  = angular.copy($scope.originalPortfolio);
            }
        };
        $scope.addOrCancelPortfolio = function(e, newPortfolio) {
            if (e.keyCode === 13) {
                var newTempPortfolio = {Number: newPortfolio.Number, Name: newPortfolio.Name};
                Portfolios.save(newTempPortfolio).$promise.then(function() {
                    $scope.initPortfolioTable();
                });
            }
            if (e.keyCode === 27) {
                $scope.newPortfolio = {Number: 1, Name: ''};
            }
        };
        $scope.setPortfolioId = function(portfolioId) {
            
            $modal.open({
                size: 'lg',
                templateUrl: 'app/components/Car/add-car.modal.html',
                controller: 'CarModalController',
                resolve: {
                    portfolioId: function() {
                        return portfolioId;
                    }
                }
            });

        };
    });
})();
(function () {
    'use strict';

    angular.module('app').factory('Portfolios', function ($resource, authService, appSettings) {
        return $resource(appSettings.apiPath + 'api/portfolio/:id', { id: '@id' },
            {
                'get': { method: 'GET' },
                'query': {
                    method: 'GET',
                    isArray: true
               },
                'save': { method: 'POST' },
                'remove': { method: 'DELETE'},
                'update': { method: 'PUT' }
            });
    });
})();
