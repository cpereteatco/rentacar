﻿(function () {
    'use strict';

    angular.module('app', [
        'auth0',
        'angular-storage',
        'angular-jwt',
        'ngResource',
        'ui.router',
        'ui.bootstrap',
        'cgBusy',
        'ngSanitize'      
    ]);

})();