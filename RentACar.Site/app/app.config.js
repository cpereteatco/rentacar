﻿(function () {
    'use strict';

    angular.module('app')
        .constant('appSettings', {
            apiPath: 'http://localhost:31159/'
        })
        .config([
            '$urlRouterProvider', '$stateProvider', 'authProvider', '$httpProvider', '$locationProvider', 'jwtInterceptorProvider',
            function($urlRouterProvider, $stateProvider, authProvider, $httpProvider, $locationProvider,
                jwtInterceptorProvider) {
                $urlRouterProvider.otherwise('/');

                $stateProvider
                    .state('home', {
                        url: '/',
                        templateUrl: 'app/components/home/home.html',
                        controller: 'HomeController',
                        requiresLogin: true
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'app/components/Login/login.html',
                        controller: 'LoginController'
                    })
                    .state('management', {
                        url: '/management',
                        templateUrl: 'app/components/Management/management.html',
                        controller: 'ManagementController'
                    })
                    .state('management.portfolio', {
                        url: '/portfolio',
                        templateUrl: 'app/components/Portfolio/portfolio-name_list.html',
                        controller: 'PortfolioController'
                    })
                    .state('management.cars', {
                        url: '/cars/:portfolioId',
                        templateUrl: 'app/components/Car/portfolio-cars-list.html',
                        controller: 'CarController'
                    })
                    .state('cars', {
                        url: '/viewCars',
                        templateUrl: 'app/components/Car/cars-list.html',
                        controller: 'CarController'
                    });

                authProvider.init({
                    domain: 'cpereteatco.eu.auth0.com',
                    clientID: 'qrXNCWbwZkfG2VFqDwlWxiTttv6MIhwv',
                    loginUrl: '/login',
                    loginState: 'login'
                });
                var refreshingToken = null;
                jwtInterceptorProvider.tokenGetter = function(store, jwtHelper) {
                    var token = store.get('token');
                    var refreshToken = store.get('refreshToken');
                    if (token) {
                        if (!jwtHelper.isTokenExpired(token)) {
                            return store.get('token');
                        } else {
                            if (refreshingToken === null) {
                                refreshingToken = auth.refreshIdToken(refreshToken).then(function(idToken) {
                                    store.set('token', idToken);
                                    return idToken;
                                }).finally(function() {
                                    refreshingToken = null;
                                });
                            }
                            return refreshingToken;
                        }
                    }
                };
                
                $httpProvider.interceptors.push('jwtInterceptor');
            }
        ])
.run(function ($rootScope, auth, store, jwtHelper, $state) {
    var refreshingToken = null;
            $rootScope.$on('$locationChangeStart', function(event,toState) {
                var token = store.get('token');
                var refreshToken = store.get('refreshToken');

                if (token) {
                    if (!jwtHelper.isTokenExpired(token)) {
                        if (!auth.isAuthenticated) {
                            $rootScope.isAuthenticated = true;
                            
                            auth.authenticate(store.get('profile'), token);
                            if ($rootScope.isAuthenticated) {
                                var profileIn = store.get('profile');
                                var profileRole = profileIn.authorization.groups[0];
                                if (profileRole === 'User') {
                                    $rootScope.isAdmin = false;

                                } else {
                                    $rootScope.isAdmin = true;
                                }                               
                            }
                        }
                    } else {
                        if (refreshToken) {
                            if (refreshingToken === null) {
                                refreshingToken = auth.refreshIdToken(refreshToken).then(function(idToken) {
                                    store.set('token', idToken);
                                    auth.authenticate(store.get('profile'), idToken);
                                }).finally(function() {
                                    refreshingToken = null;
                                });
                            }
                           
                            return refreshingToken;
                        } else {
                            $state.go('login');
                        }
                    }
                }
            });
    auth.hookEvents();
});
})();
