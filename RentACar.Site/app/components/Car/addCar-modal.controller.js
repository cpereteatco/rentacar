﻿(function () {
    'use strict';

    angular.module('app').controller('CarModalController', function ($scope, $modal, Cars, carService, portfolioId,$modalInstance) {

        $scope.car = {
            PortfolioId: portfolioId,
            State: 'selected',
            Year:'selected'

        };

        $scope.carStatesList = [
        { name: 'New' },
        { name: 'Old' }
        ];

        $scope.years = [];
        for (var currentYear = new Date().getFullYear(), i = currentYear; currentYear - 40 < i; $scope.years.push(i--));

        

        $scope.submitForm = function () {
            Cars.save($scope.car).$promise.then(function () {

                $modalInstance.close();
            });
           
        };
        $scope.cancelForm = function() {
            $modalInstance.dismiss();
        };
    });
})();