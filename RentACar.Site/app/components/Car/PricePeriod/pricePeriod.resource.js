﻿(function () {
    'use strict';

    angular.module('app').factory('PricePeriod', function ($resource, authService, appSettings) {
        return $resource(appSettings.apiPath + 'api/priceperiod/:id', { id: '@id' },
            {
                'get': { method: 'GET' },
                'query': {
                    method: 'GET',
                    isArray: true
                },
                'save': { method: 'POST' },
                'remove': { method: 'DELETE' },
                'update': { method: 'PUT' }
            });
    });
})();
