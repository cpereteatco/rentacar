﻿(function () {
    'use strict';

    angular.module('app').factory('pricePeriodService', function (PricePeriod) {
        var pricePeriodService = {};
        pricePeriodService.pp = function () {
            return PricePeriod.query();
        };
        pricePeriodService.pp = function (id) {
            return PricePeriod.get({ id: id });
        };

        pricePeriodService.ppByCarId = function (carId) {
            return PricePeriod.query({ carId: carId });
        };

        return pricePeriodService;
    });

})();