﻿(function () {
    'use strict';

    angular.module('app').controller('EditCarController', function ($scope, $modalInstance, $modal, car, PricePeriod, pricePeriodService) {
        $scope.car = car;
        $scope.isDisabled = true;
        
        $scope.promise = pricePeriodService.ppByCarId(car.CarId)
            .$promise.then(function(pp) {
                $scope.pricePeriods = pp;
            });
        $scope.years = [];
        for (var currentYear = new Date().getFullYear(), i = currentYear; currentYear - 40 < i; $scope.years.push(i--));
        $scope.carStatesList = [
            { name: 'New' },
            { name: 'Old' }
        ];

        $scope.enableSubmit = function() {
            $scope.isDisabled = false;
        };
       
        $scope.calendar = {
            opened: {},
            dateFormat: 'MM/dd/yyyy',
            dateOptions: {},
            open: function($event, which) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendar.opened[which] = true;
            }
        };
        $scope.addpriceperiod = function (car) {
            var carId = car.CarId;
            console.log(carId);
            var pricePeriod = {
                PriceValue: $scope.priceValue, StartDatePeriod: $scope.periodstartDate, EndDatePeriod: $scope.periodendDate, CarID: carId,
                PeriodID:0,PriceID:0,PricePeriodId:0
            };
            PricePeriod.save(pricePeriod).$promise.then(function () {
                $scope.periodstartDate = '';
                $scope.periodendDate = '';
                    $scope.priceValue = '';
                })
            .catch(function (response) {
                console.error('Not Valid data', response.status, response.data);
                $scope.periodstartDate = '';
                $scope.periodendDate = '';
                $scope.priceValue = '';
            });

        };


        $scope.submitForm=function() {
            
        };
        $scope.cancelForm = function() {
            $modalInstance.dismiss();
        };
    });
})();
