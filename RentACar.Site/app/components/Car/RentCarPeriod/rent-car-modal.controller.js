﻿(function () {
    'use strict';

    angular.module('app').controller('RentCarController', function ($scope, car, pricePeriodService, $modalInstance,RentCar) {
        $scope.car = car;
           $scope.pricePeriods = function () {
                $scope.promise = pricePeriodService.ppByCarId($scope.car.CarId).$promise.then(function (prt) {
                    $scope.pp = prt;
                });
           };
           $scope.calendar = {
               opened: {},
               dateFormat: 'MM/dd/yyyy',
               dateOptions: {},
               open: function ($event, which) {
                   $event.preventDefault();
                   $event.stopPropagation();
                   $scope.calendar.opened[which] = true;
               }
           };
        $scope.claculateTotalPrice = function(startDate, endDate) {

        };

        $scope.submitForm = function () {
            $scope.dataToSend = { CarID: $scope.car.CarId, StartDate: $scope.periodstartDate, EndDate: $scope.periodendDate };
            RentCar.save($scope.dataToSend).$promise.then(function () {

                $modalInstance.close();
            });
        };

        $scope.cancelForm = function() {
            $modalInstance.dismiss();
        };
    });
})();