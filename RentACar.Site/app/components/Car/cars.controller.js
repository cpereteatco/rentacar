﻿(function () {
    'use strict';

    angular.module('app').controller('CarController', function ($scope, $state, store, $modal,
        carService, portfolioService, $stateParams) {

        $scope.initCarsController = function () {
         
            $scope.changePortfolio = function (selectedPortfolio) {
                $scope.portfolioId = selectedPortfolio.PortfolioId;
             //   $state.go('management.cars', { portfolioId: $scope.portfolioId });
                $scope.promise = carService.carsByPortfolioId($scope.portfolioId)
                    .$promise.then(function (cars) {
                   $scope.carsByPortfolio = cars;
                });
                
            };

            var profile = store.get('profile');
            var userId = profile.user_id;
            $scope.promise = portfolioService.portfoliosByUserId(userId).$promise.then(function (prt) {
                $scope.portfolios = prt;
            });

           $scope.addCar = function() {
               $modal.open({
                   size: 'lg',
                   templateUrl: 'app/components/Car/add-car.modal.html',
                   controller: 'CarModalController',
                   resolve: {
                       portfolioId: function() {
                           return $scope.portfolioId;
                       }
                   }
               });
           };

            $scope.deleteCar = function(car) {
                $modal.open({
                    templateUrl: 'app/components/Car/remove-car.modal.html',
                    resolve: {
                        car: function() {
                            return car;
                        },
                        allCars: function() {
                            return $scope.carsByPortfolio;
                        }
                    },
                    controller: function($scope, $modalInstance, car, allCars, Cars) {
                        $scope.cancelForm = function() {
                            $modalInstance.dismiss();
                        };
                        $scope.submitForm = function() {
                            Cars.remove({ id: car.CarId }).$promise.then(function() {
                                var index = allCars.indexOf(car);

                                allCars.splice(index, 1);
                            }, function(response) {

                                var status = response.status;
                                if (status === 400) {
                                    $modal.open({
                                        templateUrl: 'app/components/Core/error.cars.modal.html ',
                                        controller: function($scope, $modalInstance) {
                                            $scope.closeForm = function() {
                                                $modalInstance.dismiss();
                                            };

                                        }
                                    });
                                }
                            });
                            $modalInstance.close();
                        };
                    }
                });
            };

            $scope.editCar = function (car) {               
                $modal.open({
                    size: 'lg',
                    templateUrl: 'app/components/Car/edit-car.modal.html',
                    controller: 'EditCarController',
                    resolve: {
                        car: function() {
                            return car;
                        }
                    }
                });
            };

            //$scope.saveOrCancelCar = function (e, car) {
            //    if (e.keyCode === 13) {
            //        Portfolios.update({ id: portfolio.PortfolioId }, portfolio).$promise.then(function () {
            //            $scope.initPortfolioTable();
            //        });
            //        portfolio.isInEdit = false;
            //    }
            //    if (e.keyCode === 27) {
            //        this.portfolio = angular.copy($scope.originalPortfolio);
            //    }
            //};
        };
        $scope.rentCar = function (car) {           
            $modal.open({
                size: 'lg',
                templateUrl: 'app/components/Car/rent-car.modal.html',
                controller: 'RentCarController',
                resolve: {
                    car: function () {
                        return car;
                    }
                }
            });
        };
        $scope.initCars = function () {        
            $scope.promise = carService.cars().$promise.then(function (prt) {
                    $scope.cars = prt;
                });
        };
       
    });
})();