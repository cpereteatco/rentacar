﻿(function () {
    'use strict';

    angular.module('app').factory('carService', function (Cars) {
        var carService = {};
        carService.cars = function () {
            return Cars.query();
        };
        carService.car = function (id) {
            return Cars.get({ id: id });
        };

        carService.carsByPortfolioName = function (portfolioName) {
            return Cars.query({ portfolioName: portfolioName });
        };
        carService.carsByPortfolioId = function (portfolioID) {
            return Cars.query({ portfolioId: portfolioID });
        };
       
        return carService;
    });

})();