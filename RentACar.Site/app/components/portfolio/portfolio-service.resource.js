﻿(function () {
    'use strict';

    angular.module('app').factory('portfolioService', function (Portfolios) {
        var portfolioService = {};
        portfolioService.portfolios = function () {
            return Portfolios.query();
        };
        portfolioService.portfolio = function (id) {
            return Portfolios.get({ id: id });
        };

        portfolioService.portfoliosByUserId = function (userId) {
            return Portfolios.query({ userId:userId });
        };
        //portfolioService.portfolio = function (portfolio) {
        //    return Portfolios.save(portfolio);
        //};

        return portfolioService;
    });

})();