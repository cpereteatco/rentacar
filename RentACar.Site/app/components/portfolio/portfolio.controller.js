﻿(function () {
    'use strict';
    angular.module('app').controller('PortfolioController', function ($scope,$modal,store, Portfolios, portfolioService) {
        $scope.initPortfolioTable = function () {
            $scope.initPortfoliosForSpecificUser();
            $scope.newPortfolio = { Name: '' };
        };
     
        $scope.initPortfolios = function () {
            $scope.promise = portfolioService.portfolios().$promise.then(function (prt) {
                $scope.portfolios = prt;
            });
        };

        $scope.initPortfoliosForSpecificUser = function () {
            var profile = store.get('profile');
            var userId = profile.user_id;
            $scope.promise = portfolioService.portfoliosByUserId(userId).$promise.then(function (prt) {
                $scope.portfoliosByUserId = prt;
            });
        };
    
  
        $scope.deletePortfolio = function(portfolio) {
            $modal.open({
                templateUrl: 'app/components/portfolio/remove-portfolio.modal.html',
                resolve: {
                    portfolio: function () {
                        return portfolio;
                    },
                    allPortfolios: function() {
                        return $scope.portfoliosByUserId;
                    }
                },
                controller: function ($scope, $modalInstance, portfolio, allPortfolios, Portfolios) {
                    $scope.cancelForm = function() {
                        $modalInstance.dismiss();
                    };
                    $scope.submitForm = function() {
                        Portfolios.remove({ id: portfolio.PortfolioId }).$promise.then(function () {
                            var index =allPortfolios.indexOf(portfolio);

                            allPortfolios.splice(index,1);
                        }, function (response) {

                            var status = response.status;
                            if (status === 400) {
                               $modal.open({
                                    templateUrl: 'app/components/Core/error.portfolios.modal.html ',
                                    controller: function ($scope, $modalInstance) {
                                        $scope.closeForm = function() {
                                            $modalInstance.dismiss();
                                        };

                                    }
                                });
                            }
                        });
                        $modalInstance.close();
                    };
                }
            });
        };
        $scope.editPortfolio = function(portfolio) {
            var nothingInEdit = $scope.editedPortfolio !== undefined;
            if (nothingInEdit) {
                $scope.editedPortfolio.isInEdit = false;
            }           
            $scope.editedPortfolio = portfolio;
            $scope.originalPortfolio = angular.copy(portfolio);
            portfolio.isInEdit = true;
        };
        $scope.saveOrCancelPortfolio = function(e, portfolio) {
            if (e.keyCode === 13) {
                Portfolios.update({id: portfolio.PortfolioId}, portfolio).$promise.then(function() {
                    $scope.initPortfolioTable();
                });
                portfolio.isInEdit = false;
            }
            if (e.keyCode === 27) {
              this.portfolio  = angular.copy($scope.originalPortfolio);
            }
        };
        $scope.addOrCancelPortfolio = function(e, newPortfolio) {
            if (e.keyCode === 13) {
                var newTempPortfolio = {Number: newPortfolio.Number, Name: newPortfolio.Name};
                Portfolios.save(newTempPortfolio).$promise.then(function() {
                    $scope.initPortfolioTable();
                });
            }
            if (e.keyCode === 27) {
                $scope.newPortfolio = {Number: 1, Name: ''};
            }
        };
        $scope.setPortfolioId = function(portfolioId) {
            
            $modal.open({
                size: 'lg',
                templateUrl: 'app/components/Car/add-car.modal.html',
                controller: 'CarModalController',
                resolve: {
                    portfolioId: function() {
                        return portfolioId;
                    }
                }
            });

        };
    });
})();