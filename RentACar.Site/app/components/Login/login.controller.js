﻿(function() {
    'use strict';

    angular.module('app').controller('LoginController', function($scope,$window,$rootScope, auth, $state, store) {

        $scope.login = function() {
            auth.signin({
                authParams: {
                    scope: 'openid profile'
                }
            }, function(profile, token, access_token, state, refresh_token) {
                store.set('profile', profile);
                store.set('token', token);
                store.set('refreshToken', refresh_token);
                $window.location.reload();
                $state.go('home');
            }, function(error) {
                console.log("There was an error", error);
            });
        };

    });
    
})();
