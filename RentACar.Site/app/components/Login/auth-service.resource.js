﻿(function() {
    'use strict';

    angular.module('app').factory('authService', function(authSession, auth,store) {
        var authService = {};
        authService.isAuthenticated = function() {
            if (!auth.isAuthenticated) {
                return false;
            } else {
                return true;
            }
        };

        authService.getRole = function() {
            var profile = store.get('profile');
            var role = profile.authorization.groups[0];
            return role;
        };
        return authService;
    });
})();
