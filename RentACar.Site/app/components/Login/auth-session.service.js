﻿(function () {
    'use strict';

    angular.module('app').service('authSession', function () {
        this.create = function (sessionId) {
            this.id = sessionId;
        };
        this.destroy = function () {
            this.id = null;
        };
        return this;
    });
})();
