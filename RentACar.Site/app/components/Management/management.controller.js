﻿(function () {
    'use strict';

    angular.module('app').controller('ManagementController', function($scope, $state, auth, $rootScope) {
        $scope.active = function(route) {
            return $state.is(route);
        };     
        $scope.tabs = [
            { heading: 'Portfolios', route: 'management.portfolio', active: true },
            { heading: 'Cars', route: 'management.cars', active: false }
        ];
           
        $scope.$on('$stateChangeSuccess', function () { // Keep the right tab highlighted if the URL changes.
           
            $scope.tabs.forEach(function (tab) {
                tab.active = $scope.active(tab.route);
            });
        });
    });
})();
