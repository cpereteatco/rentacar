﻿(function () {
    'use strict';

    angular.module('app').controller('HomeController', function ($scope, $window, authService,
        store, auth, $state, portfolioService) {
     
        $scope.initHomeCtrl = function () {
          
            $scope.interval = portfolioService.portfolios().$promise.then(function (prt) {
                $scope.portfolios = prt;
            });
           
        };

        $scope.selectPortfolio = function (portfolio) {
            $state.go('cars');
            store.set('portfolioID', portfolio.PortfolioId);
        };

        $scope.logout = function() {
            auth.signout();
            store.remove('profile');
            store.remove('token');
            store.remove('refreshToken');
            $window.location.reload();
        };
    });
   

})();