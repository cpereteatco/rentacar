﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.Models.Models.Mappings
{
    public class RentCarPeriodsMap : EntityTypeConfiguration<RentCarPeriods>
    {
        public RentCarPeriodsMap()
        {
            // Primary Key
            this.HasKey(t => t.RentCarPeriodId);

            // Properties
            this.Property(t => t.CarAviability)
                .HasColumnName("CarAviability")
                .IsRequired();

            this.Property(t => t.CarID)
                .HasColumnName("CarID")
                .IsRequired();

            this.Property(t => t.UserId)
               .HasColumnName("UserId")
               .IsRequired();

            this.Property(t => t.NumberOfDays)
                .HasColumnName("NumberOfDays");
                

            this.Property(t => t.StartDate)
                .HasColumnName("StartDate")
                .IsRequired();

            this.Property(t => t.EndDate)
                .HasColumnName("EndDate")
                .IsRequired();

            this.Property(t => t.TotalPrice)
                .HasColumnName("TotalPrice")
                .IsRequired();

            this.ToTable("RentCarPeriods");
        }
    }
}
