﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class PriceMap : EntityTypeConfiguration<Price>
    {
        public PriceMap()
        {
            HasKey(t => t.PriceId);
            Property(t => t.PriceValue)
                .HasColumnName("PriceValue")
                .IsRequired();

            ToTable("Price");
        }
    }
}
