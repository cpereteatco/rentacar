﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class UserRolesMap: EntityTypeConfiguration<UserRoles>
    {
        public UserRolesMap()
        {
            // Primary Key
            HasKey(t => t.UserRoleId);

            Property(t => t.UserID)
                .HasColumnName("UserID")
                .IsRequired();

            Property(t=>t.RoleID)
               .HasColumnName("RoleID")
                .IsRequired();

            ToTable("UserRoles"); 
        }
    }
}
