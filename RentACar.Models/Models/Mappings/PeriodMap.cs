﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class PeriodMap : EntityTypeConfiguration<Period>
    {
        public PeriodMap()
        {
            HasKey(t => t.PeriodId);

            this.Property(t => t.StartDate)
                .HasColumnName("StartDate")
                .HasColumnType("DATETIME")
                .IsRequired();

               this.Property(t => t.EndDate)
               .HasColumnName("EndDate")
               .HasColumnType("DATETIME")
               .IsRequired();

            this.ToTable("Period");
        }
    }
}
