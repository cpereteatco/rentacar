﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class CommentMap :EntityTypeConfiguration<Comment>
    {
        public CommentMap()
        {
            HasKey(t => t.CommentId);

            Property(t => t.Auth0UserId)
                .HasColumnName("AddedByUserID")
                .IsOptional();

            Property(t => t.CarID)
                .HasColumnName("CarID")
                .IsOptional();

            Property(t => t.CreatedOn)
                .HasColumnName("CreatedOn")
                .HasColumnType("DATETIME")
                .IsRequired();

            Property(t => t.Text)
                .HasColumnName("Text")
                .HasMaxLength(500)
                .IsRequired();

            this.ToTable("Comment");
        }
    }
}
