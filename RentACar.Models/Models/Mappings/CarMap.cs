﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.Models.Models.Mappings
{
    public class CarMap : EntityTypeConfiguration<Car>
    {
        public CarMap()
        {
            // Primary Key
            this.HasKey(t => t.CarId);

            // Properties
            this.Property(t => t.Model)
                .HasColumnName("Model");

            this.Property(t => t.Year)
                .HasColumnName("Year")
                .IsRequired();
            this.Property(t => t.Name)
               .HasColumnName("Name")
               .IsRequired();

            this.Property(t => t.SubmittedDate)
                .HasColumnName("SubmittedDate")
                .HasColumnType("DATETIME")
                .IsRequired();

            this.Property(t => t.Type)
                .HasColumnName("Type")
                .HasColumnType("VARCHAR")
                .HasMaxLength(500)
                .IsRequired();

            this.Property(t => t.ModifiedDate)
                .HasColumnName("ModifiedDate")
                .HasColumnType("DATETIME")
                .IsOptional();

            this.Property(t => t.IsAvailable)
                .HasColumnName("IsAvailable")
                .IsRequired();


            this.Property(t => t.PortfolioID)
                .HasColumnName("PortfolioID")
                .IsRequired();

            this.Property(t => t.State)
                .HasColumnName("State")
                .HasColumnType("VARCHAR")
                .HasMaxLength(500)
                .IsOptional();

            this.ToTable("Car");
        }
    }
}

