﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class RoleMap :EntityTypeConfiguration<Role>
    {
        public RoleMap()
        {
            HasKey(t => t.RoleId);
            Property(t => t.Description)
                .HasColumnName("Description")
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(50)
                .IsRequired();

            ToTable("Role");

        }
    }
}
