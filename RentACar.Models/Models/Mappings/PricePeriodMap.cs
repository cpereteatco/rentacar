﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class PricePeriodMap : EntityTypeConfiguration<PricePeriod>
    {
        public PricePeriodMap()
        {
            HasKey(t => t.PricePeriodId);

            Property(t => t.PeriodID)
                .HasColumnName("PeriodID")
                .IsRequired();

            Property(t => t.PriceID)
                .HasColumnName("PriceID")
                .IsRequired();

            Property(t => t.UserId)
                .HasColumnName("UserID")
                .IsRequired();

            Property(t => t.CarID)
              .HasColumnName("CarID")
              .IsRequired();

            ToTable("PricePeriod");
        }
    }
}
