﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class CarPhotoMap: EntityTypeConfiguration<CarPhoto>
    {
        public CarPhotoMap()
        {
            HasKey(t => t.PhotoId);

            Property(t => t.CarID)
                .HasColumnName("CarID")
                .IsRequired();

            Property(t => t.FileName)
                .HasColumnName("FileName")
                .HasMaxLength(100)
                .IsRequired();

            Property(t => t.FileType)
                .HasColumnName("FileType")
                .HasMaxLength(50)
                .IsRequired();

            ToTable("CarPhoto");
        }
    }
}
