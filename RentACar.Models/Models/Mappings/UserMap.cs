﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class UserMap: EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            // Primary Key
            HasKey(t => t.UserId);

            // Properties
            Property(t => t.Name)
                .HasColumnType("VARCHAR")
                .HasMaxLength(50)
                .IsRequired();

            Property(t => t.Email)
               .HasColumnType("VARCHAR")
               .HasMaxLength(50)
               .IsRequired();

            Property(t => t.PasswordHash)
                .HasColumnName("PasswordHash")
                .IsRequired();

            // Table & Column Mappings
            ToTable("Users");

          
        }
    }
}
