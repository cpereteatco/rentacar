﻿using System.Data.Entity.ModelConfiguration;

namespace RentACar.Models.Models.Mappings
{
    public class PortfolioMap : EntityTypeConfiguration<Portfolio>
    {
        public PortfolioMap()
        {
            HasKey(t => t.PortfolioId);

            Property(t => t.CreatedDate)
                .HasColumnName("CreatedDate")
                .HasColumnType("DATETIME")
                .IsRequired();

            Property(t => t.ModifieDate)
                .HasColumnName("ModifiedDate")
                .HasColumnType("DATETIME")
                .IsOptional();

            Property(t => t.UserID)
                .HasColumnName("UserID")
                .IsRequired();

            Property(t => t.Name)
                .HasColumnName("Name")
                .HasMaxLength(100)
                .IsRequired();

            ToTable("Portfolio");
        }
    }
}
