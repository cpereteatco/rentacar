﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentACar.Models.Models
{
    public class Car
    {     
        public Car()
        {
            CarPhotos = new List<CarPhoto>();
            Comments = new List<Comment>();
            PricePeriods = new List<PricePeriod>();
        }
        public int CarId { get; set; }
        public DateTime SubmittedDate { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public string Model { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public DateTime ModifiedDate { get; set; }
        public bool IsAvailable { get; set; }

        public virtual ICollection<CarPhoto> CarPhotos { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<PricePeriod> PricePeriods { get; set; }

        public int PortfolioID { get; set; }

        [ForeignKey("PortfolioID")]
        public virtual Portfolio Portfolio { get; set; }
    }
}
