﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentACar.Models.Models
{
    public class CarPhoto
    {
        public Guid PhotoId { get; set; }
        public string FileName { get; set; }
        public string FileType { get; set; }
        public int CarID { get; set; }
        [ForeignKey("CarID")]
        public virtual Car Car { get; set; }
    }
}