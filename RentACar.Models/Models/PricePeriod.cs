﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RentACar.Models.Models
{
    public class PricePeriod
    {
        public int PricePeriodId { get; set; }      
       
        public int PriceID { get; set; }

        [ForeignKey("PriceID")]
        public virtual Price Price { get; set; }
            
        public int PeriodID { get; set; }

        [ForeignKey("PeriodID")]
        public virtual Period Period { get; set; }

        public int CarID { get; set; }

        [ForeignKey("CarID")]
        public virtual Car Car { get; set; }
        public string UserId { get; set; }

    }
}
