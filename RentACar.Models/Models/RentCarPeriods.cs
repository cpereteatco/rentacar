﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RentACar.Models.Models
{
    public class RentCarPeriods
    {
        public int RentCarPeriodId { get; set; }
        public bool CarAviability { get; set; }
        public int  NumberOfDays { get; set; }
        public double TotalPrice { get; set; }
        public int CarID { get; set; }
        [ForeignKey("CarID")]
        public virtual Car Car { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UserId { get; set; }

    }
}
