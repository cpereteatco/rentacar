﻿using System;

namespace RentACar.Models.Models
{
    public class Period
    {
        public int PeriodId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
