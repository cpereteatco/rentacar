﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace RentACar.Models.Models
{
    public class User
    {
        public User()
        {
            Portfolios = new List<Portfolio>();
        }

        public int UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }

        public virtual ICollection<Portfolio> Portfolios { get; set; }
    }
}
