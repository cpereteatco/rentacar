﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentACar.Models.Models
{
    public class Portfolio
    {
        public Portfolio()
        {
            Cars = new List<Car>();
        }

        public int PortfolioId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifieDate { get; set; }
        public  string Name { get; set; }

        public string UserID { get; set; }

        public virtual ICollection<Car> Cars { get; set; }

    }
}
