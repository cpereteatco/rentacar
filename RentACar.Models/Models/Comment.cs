using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentACar.Models.Models
{
    public class Comment
    {
        public Comment()
        {
            CreatedOn = DateTime.Now.ToUniversalTime();
        }
        [Key]
        public int CommentId { get; set; }

        public string Text { get; set; }

        public DateTime CreatedOn { get; set; }

        [ForeignKey("CarID")]
        public Car Car { get; set; }
        public int CarID { get; set; }

        public string Auth0UserId { get; set; }

    }
}