﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace RentACar.Models.Models
{
    public class UserRoles
    {
        public int UserRoleId { get; set; }

      
        public int RoleID { get; set; }
        [ForeignKey("RoleID")]
        public virtual Role Roles { get; set; }

       
        public int UserID { get; set; }
        [ForeignKey("UserID")]
        public virtual User Users { get; set; }
    }
}
