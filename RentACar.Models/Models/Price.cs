﻿namespace RentACar.Models.Models
{
    public class Price
    {
        public int PriceId { get; set; }
        public double PriceValue { get; set; }
    }
}
